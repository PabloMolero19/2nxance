<?php

namespace App\Models;

use CodeIgniter\Model;

class PerifericoModel extends Model {

    protected $table = 'periferico';
    protected $allowedFields = ['periferico_id', 'referencia', 'tipo', 'marca', 'modelo', 'nom_comercial', 'precio'];
    protected $returnType = 'array';
    protected $validationRules = [
        'tipo' => 'required|min_length[1]|max_length[20]',
        'marca' => 'required|min_length[1]|max_length[20]',
        'modelo' => 'required|min_length[1]|max_length[50]',
        'nom_comercial' => 'required|min_length[5]|max_length[70]',
        'precio' => 'required|min_length[1]|max_length[10]|numeric'
    ];

}
