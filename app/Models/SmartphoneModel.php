<?php

namespace App\Models;

use CodeIgniter\Model;

class SmartphoneModel extends Model {

    protected $table = 'movil';
    protected $allowedFields = ['movil_id', 'referencia', 'marca', 'modelo', 'nom_comercial', 'so', 'cpu', 'ram', 'almacenamiento', 'pantalla', 'bateria', 'precio'];
    protected $returnType = 'array';
    protected $validationRules = [
        'marca' => 'required|min_length[1]|max_length[20]',
        'modelo' => 'required|min_length[1]|max_length[50]',
        'nom_comercial' => 'required|min_length[5]|max_length[70]',
        'so' => 'required|min_length[2]|max_length[15]',
        'cpu' => 'required|min_length[5]|max_length[30]',
        'ram' => 'required|min_length[5]|max_length[30]',
        'almacenamiento' => 'required|min_length[5]|max_length[50]',
        'pantalla' => 'required|min_length[2]|max_length[15]',
        'bateria' => 'required|min_length[2]|max_length[70]',
        'precio' => 'required|min_length[1]|max_length[10]|numeric'
    ];

}
