<?php

namespace App\Models;

use CodeIgniter\Model;

class ContactoModel extends Model {

    protected $table = 'contacto';
    protected $allowedFields = ['contacto_id', 'nombre', 'apellidos', 'email', 'username'];
    protected $primaryKey = 'contacto_id';
    protected $returnType = 'array';
    protected $validationRules = [
        'nombre' => 'required|min_length[1]|max_length[50]',
        'apellidos' => 'required|min_length[1]|max_length[100]',
        'email' => 'required|min_length[5]|max_length[100]|valid_email',
        'username' => 'required|min_length[1]|max_length[50]',
    ];

}
