<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model {

    protected $table = 'users';
    protected $allowfields = ['id', 'username', 'password', 'email', 'first_name', 'last_name', 'company', 'phone', 'active', 'last_login', 'created_on', 'remember_code'
        , 'remember_selector', 'forgotten_password_time', 'forgotten_password_code', 'forgotten_password_selector', 'activation_code', 'activation_selector'];
    protected $returnType = 'array';

}
