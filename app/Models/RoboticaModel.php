<?php

namespace App\Models;

use CodeIgniter\Model;

class RoboticaModel extends Model {

    protected $table = 'robotica';
    protected $allowedFields = ['robotica_id', 'referencia', 'tipo', 'modelo', 'nom_comercial', 'precio'];
    protected $returnType = 'array';
    protected $validationRules = [
        'tipo' => 'required|min_length[1]|max_length[20]',
        'marca' => 'required|min_length[1]|max_length[20]',
        'modelo' => 'required|min_length[1]|max_length[50]',
        'nom_comercial' => 'required|min_length[5]|max_length[70]',
        'precio' => 'required|min_length[1]|max_length[10]|numeric'
    ];

}
