<h1><?php echo lang('Auth.login_heading'); ?></h1>
<p><?php echo lang('Auth.login_subheading'); ?></p>

<div id="infoMessage"><?php echo $message; ?></div>

<?php echo form_open('auth/login'); ?>

<p>
    <?php echo form_label(lang('Auth.login_identity_label'), 'identity'); ?>
    <?php echo form_input($identity); ?>
</p>

<p>
    <?php echo form_label(lang('Auth.login_password_label'), 'password'); ?>
    <?php echo form_input($password); ?>
</p>

<p>
    <?php echo form_label(lang('Auth.login_remember_label'), 'remember'); ?>
    <?php echo form_checkbox('remember', '1', false, 'id="remember"'); ?>
</p>


<p><?php echo form_submit('submit', lang('Auth.login_submit_btn')); ?></p>

<?php echo form_close(); ?>

<p><a href="forgot_password"><?php echo lang('Auth.login_forgot_password'); ?></a></p>

<!-- p><button><a href="<? site_url('tiendaController') ?>" >O accede sin registrarte a la web</a></button></p> -->
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="icon" href="<?= base_url('fotos/5.ico') ?>" type="image/x-icon">

        <title><?= $pantalla ?></title>
    </head>

    <style>

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h2 style="text-align: center;">¡Regístrate en 2nXance!</h2>
        <div class="container" id="alineacion">
            <form method="post" action="<?= site_url("tiendaController/registro") ?>">
                <div class="alert alert-danger" role="alert">
                    <?php echo $message; ?>
                </div>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<?php echo form_label(lang('Auth.login_identity_label'), 'identity'); ?>
                    <?php echo form_input($identity); ?>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <?php echo form_label(lang('Auth.login_password_label'), 'password'); ?>
                    <?php echo form_input($password); ?>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <a href="forgot_password"><?php echo lang('Auth.login_forgot_password'); ?></a>
                </div>
                <div class="form-group" style="width:450px;">
                    <?php echo form_label(lang('Auth.login_remember_label'), 'remember'); ?>
                    <?php echo form_checkbox('remember', '1', false, 'id="remember"'); ?>
                </div> <br>
                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; <?php echo form_submit('submit', lang('Auth.login_submit_btn')); ?></button>
            </form>
            <button class="btn btn-secondary" style="margin-left:46%; color:white;"> <a href="<?= site_url('tiendaController/tienda') ?>" style="color:white;">Volver al home de tienda</a></button><br>
        </div>
    </body>
</html>

