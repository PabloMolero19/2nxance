<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>2nXance</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?= base_url('shop/assets/icono1.ico')?>" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?= base_url('shop/css/styles.css')?>" rel="stylesheet" />
    </head>
    <body>
        <?php $session = \Config\Services::session(); ?>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="<?= site_url('tiendaController')?>"><span class="text text-warning">2nXance</span></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="<?=site_url('tiendaController')?>">Home</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Tienda</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/productos')?>">Todos los productos</a></li>
                                <li><hr class="dropdown-divider" /></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/sobremesa')?>">Pc's sobremesa</a></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/portatil')?>">Pc's portátiles</a></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/smartphones')?>">Smartphones</a></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/tableta')?>">Tabletas</a></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/periferico')?>">Periféricos</a></li>
                                <li><a class="dropdown-item" href="<?=site_url('tiendaController/robotica')?>">Robótica</a></li>
                            </ul>
                        </li>
                        
                        <li class="nav-item"><a class="nav-link" href="<?=site_url('tiendaController/contacto')?>">Contacto</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?=site_url('tiendaController/faq')?>">Ayuda</a></li>
                        
                    </ul>
                    <form class="d-flex">
                        <a class="btn btn-outline-light" href='<?= site_url('tiendaController/muestracarro')?>'>
                            <i class="bi-cart-fill me-1" style="color: white;"></i>
                            <span style="color: white;">Carrito</span>
                            <?php
                               $articulos=0;
                               if ($session->has('carro')) {
                                   foreach($session->carro as $tipocomponente){
                                       foreach ($tipocomponente as $componente){
                                            $articulos += $componente;
                                       }
                                   }
                               }
                            ?>
                            <span class="badge bg-light text-dark ms-1 rounded-pill"><?=$articulos?></span>
                        </a>
                    </form>
                </div>
            </div>
        </nav>

            <?= $this->renderSection('content') ?>
        
        <!-- Footer-->
        <footer class="py-5 bg-secondary">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; 2nXance 2022 | Todos los derechos reservados</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?= base_url('shop/js/scripts.js')?>"></script>
    </body>
</html>
