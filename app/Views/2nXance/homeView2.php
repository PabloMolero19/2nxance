
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>2nXance - Home</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="<?= base_url('assets/icono1.ico') ?>" rel="icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="<?= base_url('home2/vendor/aos/aos.css') ?>" rel="stylesheet">
        <link href="<?= base_url('home2/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('home2/vendor/bootstrap-icons/bootstrap-icons.css') ?>" rel="stylesheet">
        <link href="<?= base_url('home2/vendor/boxicons/css/boxicons.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('home2/vendor/glightbox/css/glightbox.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('home2/vendor/swiper/swiper-bundle.min.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">

        <!-- Template Main CSS File -->
        <link href="<?= base_url('css/estilo_home.css') ?>" rel="stylesheet">


    </head>

    <body>

        <!-- ======= Top Bar ======= -->
        <section id="topbar" class="d-flex align-items-center">
            <div class="container d-flex justify-content-center justify-content-md-between">
                <div class="contact-info d-flex align-items-center">
                    <i class="bi bi-envelope-fill"></i><a href="mailto:contact@example.com">contacto@secondchance.com</a>
                    <i class="bi bi-phone-fill phone-icon"></i> +34 633 161 632
                </div>
                <div class="social-links d-none d-md-block">
                    <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                    <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
                </div>
            </div>
        </section>

        <!-- ======= Header ======= -->
        <header id="header" class="d-flex align-items-center">
            <div class="container d-flex align-items-center justify-content-between">

                <h1 class="logo"><a href="">2nXance</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

                <nav id="navbar" class="navbar">
                    <ul>
                        <li><a class="nav-link scrollto" href="#hero">Home</a></li>
                        <li><a class="nav-link scrollto" href="#about">Sobre Nosotros</a></li>
                        <li><a class="nav-link scrollto" href="#services">Servicios</a></li>
                        <li><a class="nav-link scrollto " href="#portfolio">Productos</a></li>
                        <li class="dropdown"><a href="<?= site_url('tiendaController/tienda') ?>"><span>Tienda</span> <i class="bi bi-chevron-down"></i></a>
                            <ul>
                                <li><a href="<?= site_url('tiendaController/sobremesa') ?>">Pc's sobremesa</a></li>
                                <li><a href="<?= site_url('tiendaController/portatil') ?>">Pc's portátiles</a></li>
                                <li><a href="<?= site_url('tiendaController/smartphones') ?>">Smartphones</a></li>
                                <li><a href="<?= site_url('tiendaController/tableta') ?>">Tabletas</a></li>
                                <li><a href="<?= site_url('tiendaController/periferico') ?>">Periféricos</a></li>
                                <li><a href="<?= site_url('tiendaController/robotica') ?>">Robótica</a></li>
                                <?php $this->auth = new \IonAuth\Libraries\IonAuth(); ?>
                                <?php if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))): ?>
                                    <li class="dropdown"><a href="#"><span>Subir productos</span> <i class="bi bi-chevron-right"></i></a>
                                        <ul>
                                            <li><a href="<?= site_url('tiendaController/insertarSobremesa') ?>">Pc's sobremesa</a></li>
                                            <li><a href="<?= site_url('tiendaController/insertarPortatil') ?>">Pc's portátiles</a></li>
                                            <li><a href="<?= site_url('tiendaController/insertarSmartphones') ?>">Smartphones</a></li>
                                            <li><a href="<?= site_url('tiendaController/insertarTableta') ?>">Tabletas</a></li>
                                            <li><a href="<?= site_url('tiendaController/insertarPeriferico') ?>">Periféricos</a></li>
                                            <li><a href="<?= site_url('tiendaController/insertarRobotica') ?>">Robótica</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="nav-link scrollto" href="#contact">Contacto</a></li>
                            <li><a class="nav-link scrollto" href="<?= site_url('tiendaController/faq') ?>">Ayuda</a></li>

                        <?php endif; ?>
                    </ul>
                    <i class="bi bi-list mobile-nav-toggle"></i>
                </nav><!-- .navbar -->

            </div>
        </header><!-- End Header -->

        <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex align-items-center">
            <div class="container position-relative" data-aos="fade-up" data-aos-delay="500">
                <h1 style="color:#f5cc00;">Bienvenido a 2nXance</h1>
                <h2 style="color: #787878;">Tu tienda de electrónica e informática reacondicionada de confianza</h2>
                <a href="<?= site_url('tiendaController/tienda') ?>" class="btn-get-started scrollto">Ir a la tienda</a>
            </div>
        </section><!-- End Hero -->

        <main id="main">

            <!-- ======= About Section ======= -->
            <section id="about" class="about">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left">
                            <img src="<?= base_url('assets/img/home/about.jpg') ?>" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right">
                            <h3>2nXance, líderes en el mercado de venta de electrónica e informática en España.</h3><br>
                            <p class="fst-italic">
                                eldiario.nz: "2nXance es la revolución que el sector tecnológico necesitaba. Tienda online de calidad - precio excelsa y con una atención al cliente 10/10."
                            </p><br>
                            <p>
                                2nXance es una tienda online que se dedica a la compra - venta de productos electrónicos de segunda mano. Nosotros compramos a empresas o particulares cualquier tipo de aparato electrónico, el equipo de profesionales dedicado a la
                                reparación lo dejan a punto para ser usado y lo ponemos a la venta en nuestra web.
                                Desde 2nXance hemos apostado por los reacondicionados por 3 simples razones: &darr;&nbsp;&darr;
                            </p>
                        </div>
                    </div>

                </div>
            </section><!-- End About Section -->

            <!-- ======= Why Us Section ======= -->
            <section id="why-us" class="why-us">
                <div class="container">

                    <div class="row">

                        <div class="col-lg-4" data-aos="fade-up">
                            <div class="box">
                                <span class="text-dark">01</span>
                                <h4>Sostenibilidad medioambiental</h4>
                                <p>En 2nXance, al apostar por los productos reacondicionados, no consumimos nuevos recursos medioambientales; por tanto, favorecemos a su correcta evolución y desarrollo.</p>
                            </div>
                        </div>

                        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="150">
                            <div class="box">
                                <span class="text-dark">02</span>
                                <h4>E-commerce</h4>
                                <p>Al apostar por el comercio on-line, todo el mundo puede comprar desde la comodidad de su hogar sin desplazarse físicamente a ningún lugar para tramitar el pedido.</p>
                            </div>
                        </div>

                        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
                            <div class="box">
                                <span class="text-dark">03</span>
                                <h4>Ahorro económico</h4>
                                <p>Al decidirte por comprar productos reacondicionados, el precio influye directamente sobre el subtotal de los productos; por tanto, ahorras un porcentaje económico respecto a un producto nuevo.</p>
                            </div>
                        </div>

                    </div>

                </div>
            </section><!-- End Why Us Section -->



            <!-- ======= Services Section ======= -->
            <section id="services" class="services">
                <div class="container">

                    <div class="section-title">
                        <span>Servicios</span>
                        <h2>Servicios</h2>
                        <p>En 2nXance ofrecemos una amplia variedad de servicios:</p>
                    </div>

                    <div class="row">


                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="150" id="sat">
                            <div class="icon-box">
                                <div class="icon"><i class="bx bx-file"></i></div>
                                <h4><a href="">SAT</a></h4>
                                <p>Servicio de Atención al Cliente 24h al día activo. Además, contamos con formularios de contacto en la web y unas preguntas resueltas que pueden guiarte y sacarte de dudas.</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300" id="envios">
                            <div class="icon-box">
                                <div class="icon"><i class="bx bx-tachometer"></i></div>
                                <h4><a href="">Envíos</a></h4>
                                <p>Envíos muy rápidos, siendo estos de 3 a 10 días, evidentemente dependiendo del pedido, disponibilidad, localización y tipo de usuario registrado.</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="450" id="cm">
                            <div class="icon-box">
                                <div class="icon"><i class="bx bx-world"></i></div>
                                <h4><a href="">Comercio Nacional</a></h4>
                                <p>2nXance está activo, realiza envíos y tramita pagos a nivel nacional, es decir, a toda la península ibérica (exceptuando portugal) y canarias</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="600" id="md">
                            <div class="icon-box">
                                <div class="icon"><i class="bx bx-slideshow"></i></div>
                                <h4><a href="">Marketing Digital</a></h4>
                                <p>Estamos activos en redes sociales, publicando novedades, ofertas, noticias... Además, realizaremos anuncios en televisión en un futuro.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="600" id="sp">
                            <div class="icon-box">
                                <div class="icon"><i class="bi bi-arrow-up-circle"></i></div>
                                <h4><a href="">Subida de productos</a></h4>
                                <p>Además de comprar los productos que ofertamos desde 2nXance, la plataforma te permite subir tus propios productos y venderlos.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="600" id="garantias">
                            <div class="icon-box">
                                <div class="icon"><i class="bi bi-award"></i></div>
                                <h4><a href="">Garantías</a></h4>
                                <p>Cuándo compras un producto en una tienda de primera mano, no selen ofrecer muchas garantías. En 2nXance, ofrecemos las garantías oficiales y otras ampliadas por nosotros.</p>
                            </div>
                        </div>



                    </div>

                </div>
            </section><!-- End Services Section -->

            <!-- ======= Cta Section =======  LLAMADA A LA ACCIÓN-->
            <section id="cta" class="cta">
                <div class="container" data-aos="zoom-in">

                    <div class="text-center">
                        <h3>Visita nuestra tienda</h3>
                        <p> Échale un vistazo a nuestra tienda online, disponemos de multiples categorías: ordenadores de sobremesa, portátiles, smartphones, tabltas, periféricos y robótica.</p>
                        <a class="cta-btn" href="#">Visita la tienda</a>
                    </div>

                </div>
            </section><!-- End Cta Section -->

            <!-- ======= Portfolio Section ======= -->
            <section id="portfolio" class="portfolio">
                <div class="container">

                    <div class="section-title">
                        <span>Productos y personalizaciones</span>
                        <h2>Productos y personalizaciones</h2>
                        <p>En 2nXance contamos con diversidad de productos, personalizaciones y merchandising.</p>
                    </div>

                    <div class="row" data-aos="fade-up">
                        <div class="col-lg-12 d-flex justify-content-center">
                            <ul id="portfolio-flters">
                                <li data-filter="*" class="filter-active">Todos</li>
                                <li data-filter=".filter-app">Productos</li>
                                <li data-filter=".filter-card">Merchandising</li>
                                <li data-filter=".filter-web">Personalizaciones</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="150">

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/1.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Pc's sobremesa</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/1.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 1"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="<?= base_url('assets/img/home/portfolio/per1.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Funda móvil</h4>
                                <p>Personalizaciones</p>
                                <a href="<?= base_url('assets/img/home/portfolio/per1.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Personalizaciones 3"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/2.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Pc's portátiles</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/2.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 2"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="<?= base_url('assets/img/home/portfolio/merch1.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Tarjeta socio</h4>
                                <p>Merchandising</p>
                                <a href="<?= base_url('assets/img/home/portfolio/merch1.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Merchandising 2"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="<?= base_url('assets/img/home/portfolio/per2.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Taza</h4>
                                <p>Personalizaciones</p>
                                <a href="<?= base_url('assets/img/home/portfolio/per2.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Personalizaciones 2"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/3.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Tabletas</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/3.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 3"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="<?= base_url('assets/img/home/portfolio/prod2.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Sudadera</h4>
                                <p>Merchandising</p>
                                <a href="<?= base_url('assets/img/home/portfolio/prod2.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Merchandising 3"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="<?= base_url('assets/img/home/portfolio/merch2.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Caja productos</h4>
                                <p>Merchandising</p>
                                <a href="<?= base_url('assets/img/home/portfolio/merch2.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Merchandising 1"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="<?= base_url('assets/img/home/portfolio/merch3.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Chocolatina</h4>
                                <p>Merchandising</p>
                                <a href="<?= base_url('assets/img/home/portfolio/merch3.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Merchandising 4"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>


                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="<?= base_url('assets/img/home/portfolio/per3.png') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Totebag</h4>
                                <p>Personalizaciones</p>
                                <a href="<?= base_url('assets/img/home/portfolio/per3.png') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Personalizaciones 1"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/4.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Smartphones</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/4.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 6"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/5.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Periféricos</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/5.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 4"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="<?= base_url('assets/img/home/portfolio/6.jpg') ?>" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>Robótica</h4>
                                <p>Productos</p>
                                <a href="<?= base_url('assets/img/home/portfolio/6.jpg') ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Productos 5"><i class="bi bi-eye"></i></a>

                            </div>
                        </div>

                    </div>

                </div>
            </section><!-- End Portfolio Section -->




            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">

                    <div class="section-title">
                        <span>Contacto</span>
                        <h2>Contacto</h2>
                        <p>Contacta con nosotros, ¡Estamos siempre disponibles!</p>
                    </div>

                    <div class="row" data-aos="fade-up">
                        <div class="col-lg-6">
                            <div class="info-box mb-4">
                                <i class="bx bx-map"></i>
                                <h3>Dirección</h3>
                                <p>C/ Almirante Císcar, 18, Benetusser, 46910, València.</p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="info-box  mb-4">
                                <i class="bx bx-envelope"></i>
                                <h3>Dirección de mail</h3>
                                <p>contacto@secondchance.com</p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="info-box  mb-4">
                                <i class="bx bx-phone-call"></i>
                                <h3>Llámanos</h3>
                                <p>+34 633 161 632</p>
                            </div>
                        </div>

                    </div>

                    <div class="row" data-aos="fade-up">

                        <div class="col-lg-6 ">
                            <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3082.2587394868274!2d-0.3962429844597575!3d39.41827322367698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd604e96755a4131%3A0x1e04e1d9b3d63d91!2sC.%20Almte.%20C%C3%ADscar%2C%2018%2C%2046910%20Benet%C3%BAser%2C%20Valencia!5e0!3m2!1ses!2ses!4v1653209823305!5m2!1ses!2ses" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
                        </div>

                        <div class="col-lg-6">
                            <form method="post" action="<?= site_url("tiendaController/insertarContacto") ?>" class="php-email-form">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
                                    </div>
                                    <div class="col-md-6 form-group mt-3 mt-md-0">
                                        <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos">
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Nombre de usuario">
                                </div>
                                <div class="form-group mt-3">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Dirección de mail">
                                </div>
                                <div class="my-3">
                                    <div class="loading">Loading</div>
                                    <div class="error-message">
                                        <?php if (!empty(\Config\Services::validation()->getErrors())): ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= \Config\Services::validation()->listErrors(); ?>
                                            </div>
                                        <?php endif; ?></div>
                                    <div class="sent-message">¡Gracias! Nos pondremos lo antes posible en contacto contigo.</div>
                                </div>
                                <div class="text-center"><button type="submit">Enviar mensaje</button></div>
                            </form>
                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-4 col-md-6">
                            <div class="footer-info">
                                <h3>2nXance</h3>
                                <p>
                                    C/ Almirante Císcar, 18<br>
                                    Benetusser, 46910, València.<br><br>
                                    <strong>Teléfono:</strong>+34 633 161 632<br>
                                    <strong>Email:</strong>contacto@secondchance.com<br>
                                </p>
                                <div class="social-links mt-3">
                                    <a href="" class="twitter"><i class="bx bxl-twitter"></i></a>
                                    <a href="" class="facebook"><i class="bx bxl-facebook"></i></a>
                                    <a href="" class="instagram"><i class="bx bxl-instagram"></i></a>
                                    <a href="" class="google-plus"><i class="bx bxl-skype"></i></a>
                                    <a href="" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Links útiles</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#about">Sobre nosotros</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#services">Servicios</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/tienda') ?>">Tienda</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/contacto') ?>">Contacto</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/faq') ?>">Ayuda</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Nuestros servicios</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#sat">SAT</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#envios">Envíos</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#cm">Comercio Nacional</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#md">Marketing Digtal</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#sp">Subida de productos</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#garantias">Garantías</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Nuestros productos</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/sobremesa') ?>">Pc's sobremesa</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/portatil') ?>">Pc's portátiles</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/tableta') ?>">Tabletas</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/smartphones') ?>">Smartphones</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/periferico') ?>">Periféricos</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="<?= site_url('tiendaController/robotica') ?>">Robótica</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Políticas y Términos</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Política de privacidad</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Términos de uso</a></li>
                            </ul>
                        </div>



                    </div>
                </div>
            </div>

            <div class="container">
                <div class="copyright">
                    Copyright &copy;<strong><span>2nXance</span></strong>. Todos los derechos reservados
                </div>

            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <div id="preloader"></div>

        <!-- Vendor JS Files -->
        <script src="<?= base_url('home2/vendor/aos/aos.js') ?>"></script>
        <script src="<?= base_url('home2/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
        <script src="<?= base_url('home2/vendor/glightbox/js/glightbox.min.js') ?>"></script>
        <script src="<?= base_url('home2/vendor/isotope-layout/isotope.pkgd.min.js') ?>"></script>
        <script src="<?= base_url('home2/vendor/swiper/swiper-bundle.min.js') ?>"></script>
        <script src="<?= base_url('home2/vendor/php-email-form/validate.js') ?>"></script>
        <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
        <!-- Template Main JS File -->
        <script src="<?= base_url('home2/js/main.js') ?>"></script>

    </body>

</html>