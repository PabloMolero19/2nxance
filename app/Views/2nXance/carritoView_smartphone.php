


<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <?php foreach ($smartphone as $producto): ?>
                <div class="col mb-5">
                    <div class="card h-100">
                        <!--Product image-->

                        <!--Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name -->
                                <img class="card-img-top" src="<?= base_url('assets/img/productos/smartphone/' . $producto['referencia']) ?>.jpg" width="150px">
                                <h5 class="fw-bolder"><?= $producto['nom_comercial'] ?></h5>
                                <ul>
                                    <li>CPU: <?= $producto['cpu'] ?></li>
                                    <li>RAM: <?= $producto['ram'] ?></li>
                                    <li>Pantalla: <?= $producto['pantalla'] ?></li>
                                    <li>Bateria: <?= $producto['bateria'] ?></li>
                                    <li>Almacenamiento: <?= $producto['almacenamiento'] ?></li>
                                    <li>Sistema Operativo: <?= $producto['cpu'] ?></li><br>
                                    <!-- Product price-->
                                    Precio: <?= $producto['precio'] ?>&nbsp;€</ul>
                                <hr>
                                <?= $producto['cantidad'] ?>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>

