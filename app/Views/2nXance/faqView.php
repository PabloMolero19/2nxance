<?php
$auth = new \IonAuth\Libraries\IonAuth();
?>

<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase"><span class="text-warning">2nXance</span> - FAQ</h2>
            <h3 class="section-subheading text-muted">Aquí encontraras las preguntas más frecuentes resueltas.</h3>
        </div>
        <div class="accordion w-100" id="basicAccordion">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseOne" aria-expanded="false" aria-controls="collapseOne">
                        ¿Qué es 2nXance?
                    </button>
                </h2>
                <div id="basicAccordionCollapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        2nXance es una tienda online que se dedica a la compra - venta de productos electrónicos de segunda mano. Nosotros
                        compramos a empresas o particulares cualquier tipo de aparato electrónico, el equipo de profesionales dedicado a la
                        reparación lo dejan a punto para ser usado y lo ponemos a la venta en nuestra web.
                        Desde 2nXance hemos apostado por los reacondicionados por 3 simples razones:

                        <ol>
                            <li>Sostenibilidad medioambiental.</li>
                            <li>E-commerce.</li>
                            <li>Ahorro económico.</li>
                        </ol>
                        Sé parte del cambio, sé parte de 2nXance.

                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        ¿Qué vendemos?
                    </button>
                </h2>
                <div id="basicAccordionCollapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        2nXance tiene a la venta una gran variedad de productos reacondicionados de electrónica, pero las categorías más
                        populares son:
                        <br><br><ol>
                            <li> Ordenadores de sobremesa.</li>
                            <li>Ordenadores portátiles.</li>
                            <li>Smartphones.</li>
                            <li>Tabletas.</li>
                            <li> Periféricos.</li>
                            <li> Robótica.</li>
                        </ol>
                        Si tienes dudas sobre qué productos puedes encontrar que no sean de esas categorías, por favor, contacta con el soporte
                        de 2nXance pinchando aquí.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseThree" aria-expanded="false" aria-controls="collapseThree">
                        ¿Son fiables los reacondicionados?
                    </button>
                </h2>
                <div id="basicAccordionCollapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        En efecto. Nuestro equipo de profesionales en la reparación de electrónica deja los productos como nuevos, cambiando
                        piezas, haciendo pruebas de seguridad, pruebas de rendimiento... En definitiva, son la mejor opción, porque además de
                        que ayudamos a la sostenibilidad medioambiental al no consumir nuevos componentes, nos ahorramos un dinero y te
                        otorgamos una serie de garantías que, muchas veces, la propia marca no te va a otorgar.
                        Si te interesa, puedes leer dos artículos que explican esto mucho más a fondo :
                        <ul>
                            <li><a href="https://www.ocu.org/consumo-familia/compras-online/informe/productos-reacondicionados" target="_blank">Artículo de la OCU.</a></li>
                            <li><a href="https://www.guiahardware.es/merecen-la-pena-comprar-productos-reacondicionados/" target="_blank">Artículo de Guía Hardware.</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFive">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseFive" aria-expanded="false" aria-controls="collapseFive">
                        ¿La plataforma web de 2nXance es fiable?
                    </button>
                </h2>
                <div id="basicAccordionCollapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Claro, la plataforma de 2nXance es fiable. A la hora de realizar el pago, tenemos la política de protección al comprador.
                        Nosotros no vamos a realizar el cobro hasta que la agencia de transporte nos confirme que ha entregado el paquete y, por supuesto, el cliente dispone de 15 días laborales para realizar reclamaciones. No se aceptan devoluciones de
                        productos ni de la cantidad íntegra ni parcial del subtotal, a menos de que sea por motivos ajenos a nuestra empresa.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingSix">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseSix" aria-expanded="false" aria-controls="collapseSix">
                        ¿Cómo se puede vender en 2nXance?
                    </button>
                </h2>
                <div id="basicAccordionCollapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Todo el mundo es apto para vender en nuestra tienda, ya sea una empresa, particulares o incluso una persona desde la
                        comodidad de su hogar. Para poder vender en nuestra tienda, tan solo ha de estar registrado con un usuario que
                        pertenezca al rol de "Venta", "Compra-Venta" o "VIP". Más tarde, tendrás una opción en la parte de productos que te
                        permitirá rellenar un formulario y subir el producto a la plataforma con sus características e imagen distintiva.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingSeven">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                        ¿Cómo puedo comprar en 2nXance?
                    </button>
                </h2>
                <div id="basicAccordionCollapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Todo el mundo es apto para comprar en nuestra tienda, ya sea una empresa, particulares o incluso una persona desde la
                        comodidad de su hogar. Para poder vender en nuestra tienda, tan solo ha de estar registrado con un usuario que
                        pertenezca al rol de "Compra", "Compra-Venta" o "VIP". Más tarde, en las tablas de los productos, tendrás una opción
                        que te permitirá añadir al carro cualquier elemento y, posteriormente, tramitar la compra.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingEight">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseEight" aria-expanded="false" aria-controls="collapseEight">
                        ¿Que tipos de usuarios hay en 2nXance?
                    </button>
                </h2>
                <div id="basicAccordionCollapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Actualmente, contamos con 4 tipos de usuarios, cada cual con sus diferentes características. Cabe recalcar que este tipo
                        de grupos o roles se escogen a la hora de registrarse en la web y NO se puede cambiar.
                        <br><br><ol>
                            <li>Compradores: los compradores son aquellos usuarios que únicamente se van a dedicar a realizar compras en
                                nuestra web, por tanto, no pueden tener acceso a vender sus productos.</li>
                            <li>Vendedores: los vendedores son aquellos usuarios que únicamente se van a dedicar a realizar ventas en nuestra
                                web, por tanto, no pueden tener acceso a comprar otros productos.</li>
                            <li>Compra - Venta: son una mezcla de ambos tipos de usuario. Son capaces de comprar productos y de vender
                                los suyos propios. La opción más recomendada para usuarios novatos es esta, ya que tienes ambas opciones.
                            </li>
                            <li>VIP: Los usuarios VIP son como los compra - venta pero con características adicionales: la tarjeta 2nXance,
                                que te permite envíos gratuitos, descuentos exclusivos, sorteos mensuales...</li>
                            Eso sí, tiene un coste de 4€ o de una cantidad propuesta del usuario (de 4€ en adelante) a modo de donativo mensual
                            para el mantenimiento de la página o por la cantidad de 48€ anuales realizable en un pago único.
                        </ol>
                    </div>
                </div>
            </div> 
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingNine">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseNine" aria-expanded="false" aria-controls="collapseNine">
                        ¿Cómo y cuándo llega mi paquete?
                    </button>
                </h2>
                <div id="basicAccordionCollapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Los envíos se realizan los días laborales, es decir, de lunes a viernes. Una vez preparado el pedido, lo normal es que,
                        dependiendo de la agencia de transporte y el tipo de usuario que seas, tarde de 3 a 10 días. Los paquetes se envían con
                        cajas exclusivas de 2nXance y, dependiendo del importe total del pedido, incluimos camisetas, sudaderas, gominolas...
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTen">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseTen" aria-expanded="false" aria-controls="collapseTen">
                        ¿Dónde encontrarnos?
                    </button>
                </h2>
                <div id="basicAccordionCollapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Actualmente, los pedidos los realizamos en una nave industrial en el polígono industrial de Benetússer, València, 46910,
                        en la calle Almirante Císcar número 18, pero nuestra actividad comercial tan solo está activa en nuestra web, debido a
                        que nos dedicamos única y exclusivamente al e-commerce.
                        Si quieres estar al tanto de cualquier novedad de nuestra empresa tenemos redes sociales donde vamos actualizando la
                        información:
                        <br><br><ol>
                            <li>Instagram.</li>
                            <li>Twitter.</li>
                            <li>Facebook.</li>
                            <li>LinkedIn.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingEleven">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                        ¿Qué garantías ofrecemos?
                    </button>
                </h2>
                <div id="basicAccordionCollapseEleven" class="accordion-collapse collapse" aria-labelledby="headingEleven"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        2nXance garantiza sus equipos contra cualquier defecto de reparación a partir de la entrega del producto en el periodo
                        de:
                        <br><br><ol>
                            <li>Dos años, de acuerdo a lo establecido por la Ley 23/2003, de 10 de Julio, de Garantías en la Venta de Bienes
                                de Consumo.</li>
                            <li>Un año, en el caso de productos no amparados por la mencionada Ley.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwelve">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                        ¿Qué métodos de pago aceptamos?
                    </button>
                </h2>
                <div id="basicAccordionCollapseTwelve" class="accordion-collapse collapse" aria-labelledby="headingTwelve"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Aceptamos casi cualquier método de pago actual. Pagos con tarjeta (Visa, Mastercard y American Express), pagos
                        digitales (Paypal y Paysafecard), Bizum (por importes menores a 5.000€, indicando en la operación número de pedido,
                        nombre de usuario y fecha de realización) y transferencias bancarias a la cuenta ES31 4369 3897 2312 4303 5000
                        (indicando en la operación número de pedido, nombre de usuario y fecha de realización).
                    </div>
                </div>

            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading13">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#basicAccordionCollapse13" aria-expanded="false" aria-controls="collapse13">
                        ¿Hay posibilidad de anulaciones?
                    </button>
                </h2>
                <div id="basicAccordionCollapse13" class="accordion-collapse collapse" aria-labelledby="heading13"
                     data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        Sin perjuicio de otras reclamaciones, nos reservamos el derecho a rescindir o anular cualquier operación, en el caso de
                        incumplimiento de cualquiera de las mencionadas condiciones, así como en los supuestos de impago total o parcial de
                        un pedido.
                    </div>
                </div>

            </div>

        </div>
    </div><br><br>
    <div style="display: flex; justify-content: center;">
        <a href="<?= base_url('assets/faq.pdf') ?>" download="faq-2nxance" class="btn btn-warning">
            Descargar todas las preguntas y respuestas aquí
        </a>
    </div>
</section>

<?= $this->endSection() ?>
