<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<div class="text-center" style="padding-top: 2%">
    <h2 class="section-heading text-uppercase" ><span class="text-warning">2nXance</span> - Insertar</h2>
    <h4 class="section-subheading text-muted"><?= $titulo ?></h4>
</div><br>
<div class="container" style="margin: 0 auto; padding-left: 30%;">
    <?php if (!empty(\Config\Services::validation()->getErrors())): ?>
        <div class="alert alert-danger" role="alert">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
    <?php endif; ?>

    <?= form_open_multipart(site_url("tiendaController/insertarSobremesa")) ?>

    <div class="form-group" style="width:450px;">
        <label name="marca">Marca</label>
        <input type="text" class="form-control" id="marca" name="marca" placeholder="Por ejemplo MSI">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="modelo">Modelo</label>
        <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Por ejemplo MAG META S">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="nom_comercial">Nombre Comercial</label>
        <input type="text" class="form-control" id="nom_comercial" name="nom_comercial" placeholder="Es la suma de los anteriores campos: MSI MAG META S">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="so">Sistema Operativo</label>
        <input type="text" class="form-control" id="so" name="so" placeholder="Por ejemplo W10 o Android 11">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="cpu">Procesador</label>
        <input type="text" class="form-control" id="cpu" name="cpu" placeholder="Por ejemplo AMD Ryzen 5 3600">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="ram">Memoria RAM</label>
        <input type="text" class="form-control" id="ram" name="ram" placeholder="Por ejemplo 8GB DDR4 3200Mhz">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="almacenamiento">Almacenamiento</label>
        <input type="text" class="form-control" id="almacenamiento" name="almacenamiento" placeholder="Por ejemplo 1TB NVME M.2 + 1TB HDD">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="graficos">Gráficos</label>
        <input type="text" class="form-control" id="graficos" name="graficos" placeholder="Por ejemplo RTX 3070Ti de 8GB">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="precio">Precio en €</label>
        <input type="text" class="form-control" id="precio" name="precio" placeholder="Por ejemplo 500">
    </div> <br>

    <div class="form-group" style="width:450px;">
        <?= form_upload(['name' => 'imagen', 'id' => 'imagen', 'class' => 'custom-file-control', 'onchange' => 'readURL(this);']) ?>
        <?= form_label('Selecciona la imagen', 'imagen', ['class' => 'custom-file-label']) ?>
    </div>
    <div class="form-group" style="width:450px;">
        <img id="visor" class=""  width="100%">
    </div>
</div>
<div style="margin-left:45%;" >
    <?= form_submit('Subir producto', 'Subir producto', ['class' => 'btn btn-warning']) ?>
</div><br>
<?= form_close() ?>
<a class="btn btn-outline-warning" style="margin-left:43.5%;" href="<?= site_url('tiendaController/productos') ?>">Volver al home de tienda</a><br><br>
<script type="text/javascript" src="<?= base_url('js/visor.js') ?>"></script>
<?= $this->endSection() ?>