<?php
$auth = new \IonAuth\Libraries\IonAuth();
?>
<?php $session = \Config\Services::session(); ?>

<?= $this->extend('layout/plantilla2') ?>

<?= $this->section('content') ?>
<!-- Header-->
<header class="bg-secondary py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">Carrito de la compra de <span class="text text-warning">2nXance</span></h1>
            <p class="lead fw-normal text-white-50 mb-0">Gestiona tus pedidos con nosotros</p>
        </div>
    </div>
</header>
<div style="margin-left: 69%; margin-top: 5%;">
    <a class="btn btn-outline-success" href="<?= site_url('tiendaController/productos') ?>">Seguir comprando</a>&nbsp;<a class="btn btn-outline-danger" href="<?= site_url('tiendaController/borraCarro') ?>">Vaciar Carro</a>
</div>