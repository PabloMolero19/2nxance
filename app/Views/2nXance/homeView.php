<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>2nXance - <?=$pantalla?></title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?= base_url("assets/icono1.ico")?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?= base_url("css/styles.css")?>" rel="stylesheet" />
    </head>
    <body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="2nXance" /></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ms-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                <li class="nav-item"><a class="nav-link" href="#servicios">Servicios</a></li>
                <li class="nav-item"><a class="nav-link" href="#portfolio">Productos</a></li>
                <li class="nav-item"><a class="nav-link" href="#about">Sobre Nosotros</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= site_url('tiendaController/tienda')?>" target="_blank">Tienda</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= site_url('tiendaController/contacto')?>" target="_blank">Contacto</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= site_url('tiendaController/faq')?>" target="_blank">Ayuda</a></li>

            </ul>
        </div>
    </div>
</nav>
<!-- Masthead-->
<header class="masthead">
    <div class="container">
        <div class="masthead-subheading">Tu tienda de electrónica e informática reacondicionada de confianza</div>
        <div class="masthead-heading text-uppercase">Bienvenido a 2nXance</div>
        <a class="btn btn-primary btn-xl text-uppercase" href="#servicios">Cuéntame más</a>
    </div>
</header>
<!-- Services-->
<section class="page-section" id="servicios">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Servicios</h2>
            <h3 class="section-subheading text-muted">En nuestra empresa, 2nXance, ofrecemos una gran variedad de servicios.</h3>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
                    <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="my-3">Tienda Online</h4>
                <p class="text-muted">En 2nXance miramos hacia el futuro, por eso apostamos por el E-commerce. Contamos con un apartado donde los usuarios pueden
                    subir sus propios productos y comprar los del resto de usuarios que suban a nuestra plataforma.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
                    <i class="fas fa-laptop fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="my-3">Variedad de productos</h4>
                <p class="text-muted">En nuestra tienda podrás encontrar todo tipo de productos reacondicionados.
                    Tenemos desde ordenadores hasta raspberrys pasando por móviles, tablets... ¿Porqué no echas un vistazo más abajo?</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
                    <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="my-3">Protección al comprador</h4>
                <p class="text-muted">Gracias a la tecnología de "Protección al comprador" tendrás tu dinero a salvo hasta que te llegue el producto y lo pruebes,
                    así evitamos timos o estafas de cualquier tipo.</p>
            </div>
        </div>
    </div>
</section>
<!-- Portfolio Grid-->
<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Categorías populares</h2>
            <h3 class="section-subheading text-muted">Aquí encontraras las categorías de productos más vendidas.</h3>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 mb-4">
                <!-- Portfolio item 1-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/1.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Pc's</div>
                        <div class="portfolio-caption-subheading text-muted">Ordenadores de sobremesa</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-4">
                <!-- Portfolio item 2-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/2.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Portátiles</div>
                        <div class="portfolio-caption-subheading text-muted">Ordenadores portátiles</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-4">
                <!-- Portfolio item 3-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                        <div class="portfolio-hover">portfolioModal
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/3.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Tablets</div>
                        <div class="portfolio-caption-subheading text-muted">Tabletas inteligentes</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                <!-- Portfolio item 4-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal4">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/4.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Telefonía</div>
                        <div class="portfolio-caption-subheading text-muted">Smartphones</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                <!-- Portfolio item 5-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal5">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/5.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Accesorios</div>
                        <div class="portfolio-caption-subheading text-muted">Accesorios de Ordenadores y Telefonía</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- Portfolio item 6-->
                <div class="portfolio-item">
                    <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal6">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/6.jpg") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Robótica</div>
                        <div class="portfolio-caption-subheading text-muted">Raspberrys y Arduinos</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sobre Nosotros -->
<section class="page-section" id="about">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Sobre Nosotros</h2>
            <h3 class="section-subheading text-muted">Nuestro recorrido como empresa hasta hoy en día.</h3>
        </div>
        <ul class="timeline">
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?= base_url("assets/img/about/1.jpg") ?>" alt="..." /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>2009-2011</h4>
                        <h4 class="subheading">Nuestros humildes inicios</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Empezamos un 09 de febrero del año 2009, en un garage de un adosado en Beniparrell, una localidad de la Comunidad Valenciana. Allí, empezaríamos a reparar ordenadores, móviles...</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?= base_url("assets/img/about/2.jpg") ?>" alt="..." /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Mayo 2011</h4>
                        <h4 class="subheading">Empezamos a crecer</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Como empezamos a crecer, ya no nos bastaba con un simple garage en el que reparábamos dispositivos electrónicos, así que nos mudamos al polígono industrial de Silla, una localidad de la Comunidad Valenciana para elevar nuestra producción.</p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?= base_url("assets/img/about/3.jpg") ?>" alt="..." /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Diciembre 2015</h4>
                        <h4 class="subheading">Abrimos nuestra web</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">En 2015 nace nuestra web donde venderíamos a toda la península, ya no sólo en nuestro propio local. Además, empezaríamos a implementar a nivel local el que los clientes puedan vender sus propios productos, nosotros lo reparamos y lo volvemos a vender.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="<?= base_url("assets/img/about/4.jpg") ?>" alt="..." /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Junio 2022</h4>
                        <h4 class="subheading">Nuestro máximo esplendor</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">En 2022 somos unos referentes de la reparación (y posterior venta) de productos electrónicos. También nuestra plataforma dedicada a los clientes que quieren vender sus propios productos está valorada como la 2º mejor plataforma de compra-venta online de España.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image">
                    <h4>
                        ¡Forma parte
                        <br />
                        de nuestra
                        <br />
                        Historia!
                    </h4>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- Team-->
<section class="page-section bg-light" id="team">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">¿Quiénes somos?</h2>
            <h3 class="section-subheading text-muted">Conoce a las caras visibles de nuestra empresa, 2nXance.</h3>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="<?= base_url("assets/img/team/1.jpg") ?>" alt="..." />
                    <h4>Jonathan Romero</h4>
                    <p class="text-muted">CEO de 2nXance</p>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Jonathan Romero Twitter Profile"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Jonathan Romero Facebook Profile"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Jonathan Romero LinkedIn Profile"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="<?= base_url("assets/img/team/2.jpg") ?>" alt="..." />
                    <h4>Violeta Aguilar</h4>
                    <p class="text-muted">Co CEO de 2nXance</p>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Violeta Aguilar Twitter Profile"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Violeta Aguilar Facebook Profile"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Violeta Aguilar LinkedIn Profile"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="<?= base_url("assets/img/team/3.jpg") ?>" alt="..." />
                    <h4>Juan Pablo Merino</h4>
                    <p class="text-muted">Jefe de Márketing</p>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Juan Pablo Merino Twitter Profile"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Juan Pablo Merino Facebook Profile"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Juan Pablo Merino LinkedIn Profile"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">Nuestro equipo directivo se encarga de que la empersa salga a flote de la manera más solvente posible, obteniendo la mayor cantidad de beneficios posibles.</p></div>
            <!-- PRUEBA DE CARGA IMAGEN <img class="img-fluid" src="<? base_url("assets/img/header-bg.jpg") ?>" alt="..." /> -->
        </div>
    </div>
</section>
<!-- Colaboradores -->
<div class="py-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-3 col-sm-6 my-3">
                <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="<?= base_url("assets/img/logos/microsoft.svg") ?>" alt="..." aria-label="Microsoft Logo" /></a>
            </div>
            <div class="col-md-3 col-sm-6 my-3">
                <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="<?= base_url("assets/img/logos/google.svg") ?>" alt="..." aria-label="Google Logo" /></a>
            </div>
            <div class="col-md-3 col-sm-6 my-3">
                <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="<?= base_url("assets/img/logos/facebook.svg") ?>" alt="..." aria-label="Facebook Logo" /></a>
            </div>
            <div class="col-md-3 col-sm-6 my-3">
                <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="<?= base_url("assets/img/logos/ibm.svg") ?>" alt="..." aria-label="IBM Logo" /></a>
            </div>
        </div>
    </div>
</div>
<!-- Footer-->
<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2022</div>
            <div class="col-lg-4 my-3 my-lg-0">
                <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <div class="col-lg-4 text-lg-end">
                <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
            </div>
        </div>
    </div>
</footer>
<!-- Portfolio Modals-->
<!-- Portfolio item 1 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?= base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Ordenadores de Sobremesa</h2>
                            <p class="item-intro text-muted">Aquí encontraras información sobre los ordenadores de sobremesa que tenemos en 2nXance.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/1.jpg")?>" alt="..." />
                            <p>En 2nXance nosotros trabajamos con todas las marcas de componentes, por tanto vas a poder encontrar ordenadores premontados con todo tipo de marcas diferentes, marcas iguales o, en definitiva, una vareidad inmensa de ordenadores de sobremesa que se adaptarán a tus necesidades.</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Marca del ordenador de la foto:</strong>
                                    Dell - Alienware
                                </li>
                                <li>
                                    <strong>Nombre del ordenador:</strong>
                                    Alienware Aurora
                                </li>
                                <li>
                                    <strong>Precio:</strong>
                                    2.500 €
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Cerrar ventana
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio item 2 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?=base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Ordenadores Portátiles</h2>
                            <p class="item-intro text-muted">Aquí encontraras información sobre los ordenadores portátiles que tenemos en 2nXance.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/2.jpg")?>" alt="..." />
                            <p>En 2nXance tenemos una amplia variedad de ordenadores portátiles que, seguro, se adaptan a tus necesidades. Tenemos portátiles enfocados a diseño gráfico, ofimática, gaming y <i>de todo un poco</i>...</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Marca del ordenador de la foto:</strong>
                                    Huawei
                                </li>
                                <li>
                                    <strong>Nombre del ordenador:</strong>
                                    Matebook 14
                                </li>
                                <li>
                                    <strong>Precio:</strong>
                                    750 €
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Cerrar ventana
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio item 3 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?=base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Tablets</h2>
                            <p class="item-intro text-muted">Aquí encontraras información sobre las tabletas que tenemos en 2nXance.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/3.jpg")?>" alt="..." />
                            <p>En 2nXance tenemos una amplia variedad de tablets que, seguro, se adaptan a tus necesidades. Tenemos tabletas enfocadas a diseño gráfico, ofimática, gaming y <i>de todo un poco</i>...</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Marca de la tableta de la foto:</strong>
                                    Samsung
                                </li>
                                <li>
                                    <strong>Nombre del ordenador:</strong>
                                    Galaxy Tab A7
                                </li>
                                <li>
                                    <strong>Precio:</strong>
                                    120 €
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Cerrar ventana
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio item 4 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?=base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Project Name</h2>
                            <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/4.jpg")?>" alt="..." />
                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Client:</strong>
                                    Lines
                                </li>
                                <li>
                                    <strong>Category:</strong>
                                    Branding
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Close Project
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio item 5 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?=base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Project Name</h2>
                            <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/5.jpg")?>" alt="..." />
                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Client:</strong>
                                    Southwest
                                </li>
                                <li>
                                    <strong>Category:</strong>
                                    Website Design
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Close Project
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio item 6 modal popup-->
<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal"><img src="<?=base_url("assets/img/close-icon.svg")?>" alt="Close modal" /></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <!-- Project details-->
                            <h2 class="text-uppercase">Project Name</h2>
                            <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                            <img class="img-fluid d-block mx-auto" src="<?= base_url("assets/img/portfolio/6.jpg")?>" alt="..." />
                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                            <ul class="list-inline">
                                <li>
                                    <strong>Client:</strong>
                                    Window
                                </li>
                                <li>
                                    <strong>Category:</strong>
                                    Photography
                                </li>
                            </ul>
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <i class="fas fa-xmark me-1"></i>
                                Close Project
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="<?=base_url("js/scripts.js")?>"></script>
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
<!-- * *                               SB Forms JS                               * *-->
<!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>
</html>