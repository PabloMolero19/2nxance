<?php
$auth = new \IonAuth\Libraries\IonAuth();
?>

<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<?php $session = \Config\Services::session(); ?>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="<?= site_url('tiendaController') ?>"><span class="text text-warning">2nXance</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link" aria-current="page" href="<?= site_url('tiendaController') ?>">Home</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Tienda</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/productos') ?>">Todos los productos</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/sobremesa') ?>">Pc's sobremesa</a></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/portatil') ?>">Pc's portátiles</a></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/smartphones') ?>">Smartphones</a></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/tableta') ?>">Tabletas</a></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/periferico') ?>">Periféricos</a></li>
                        <li><a class="dropdown-item" href="<?= site_url('tiendaController/robotica') ?>">Robótica</a></li>
                    </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="<?= site_url('tiendaController/contacto') ?>">Contacto</a></li>
                <li class="nav-item"><a class="nav-link" href="<?= site_url('tiendaController/faq') ?>">Ayuda</a></li>

            </ul>
            <form class="d-flex">
                <a class="btn btn-outline-light" href='<?= site_url('tiendaController/muestracarro') ?>'>
                    <i class="bi-cart-fill me-1" style="color: white;"></i>
                    <span style="color: white;">Carrito</span>
                    <?php
                    $articulos = 0;
                    if ($session->has('carro')) {
                        foreach ($session->carro as $tipocomponente) {
                            foreach ($tipocomponente as $componente) {
                                $articulos += $componente;
                            }
                        }
                    }
                    ?>
                    <span class="badge bg-light text-dark ms-1 rounded-pill"><?= $articulos ?></span>
                </a>
            </form>
        </div>
    </div>
</nav>
<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase"><span class="text-warning">2nXance</span> - Tienda</h2>
            <h3 class="section-subheading text-muted">Aquí encontraras las categorías de productos más vendidas.</h3>
        </div>
        <div class="row">
            <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('comprador') OR $auth->inGroup('compraventa') OR $auth->inGroup('VIP'))): ?>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 1-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/sobremesa') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/1.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Pc's</div>
                            <div class="portfolio-caption-subheading text-muted">Ordenadores de sobremesa</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 2-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/portatil') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/2.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Portátiles</div>
                            <div class="portfolio-caption-subheading text-muted">Ordenadores portátiles</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 3-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/tableta') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/3.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Tablets</div>
                            <div class="portfolio-caption-subheading text-muted">Tabletas inteligentes</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <!-- Portfolio item 4-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/smartphones') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/4.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Telefonía</div>
                            <div class="portfolio-caption-subheading text-muted">Smartphones</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                    <!-- Portfolio item 5-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/periferico') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/5.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Periféricos</div>
                            <div class="portfolio-caption-subheading text-muted">Accesorios de Ordenadores y Telefonía</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <!-- Portfolio item 6-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="<?= site_url('tiendaController/robotica') ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="<?= base_url("assets/img/portfolio/6.jpg") ?>" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Robótica</div>
                            <div class="portfolio-caption-subheading text-muted">Raspberrys y Arduinos</div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($auth->loggedIn() == FALSE): ?>
                <h1 style="color: red"> REGÍSTRATE O INICIA SESIÓN PARA VER EL CONTENIDO</h1>
            <?php endif; ?>
            <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('vendedor') OR $auth->inGroup('compraventa') OR $auth->inGroup('VIP'))): ?>
                <div class="portfolio-item">
                    <br>
                    <a class="portfolio-link" data-bs-toggle="modal" href="#insertarModal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="<?= base_url("assets/img/portfolio/7.png") ?>" alt="..." />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">Insertar</div>
                        <div class="portfolio-caption-subheading text-muted">Subir nuevos productos a la tienda</div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portfolio-modal modal fade" id="insertarModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-bs-dismiss="modal"><img src="<?= base_url("assets/img/close-icon.svg") ?>" alt="Close modal" /></div>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="modal-body">
                                        <!-- Project details-->
                                        <h2 class="text-uppercase">Subir nuevos productos</h2>
                                        <p class="item-intro text-muted">Seleciona a continuación la categoría a la que pertenece el producto que quieres subir:</p>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarSobremesa') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Ordenadores sobremesa</span></a>
                                            </li><br>
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarPortatil') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Ordenadores portátiles</span></a>
                                            </li><br>
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarTableta') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Tabletas</span></a>
                                            </li><br>
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarSmartphones') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Smartphones</span></a>
                                            </li><br>
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarPeriferico') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Periféricos</span></a>
                                            </li><br>
                                            <li>
                                                <a href="<?= site_url('tiendaController/insertarRobotica') ?>" class="btn btn-warning btn-lg text-uppercase"><span style="color: white;">Robótica</span></a>
                                            </li><br>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>
