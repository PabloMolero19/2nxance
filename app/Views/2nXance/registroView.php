<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="icon" href="<?= base_url("assets/icono1.ico") ?>" type="image/x-icon">

        <title><?= $pantalla ?></title>
    </head>

    <style>

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h2 style="text-align: center;">¡Regístrate en 2nXance!</h2>
        <h5 style="text-align: center;"><i><a href="<?= site_url('auth/login') ?>" style="text-decoration:none;" class="text-muted">Ya tengo cuenta</a></i></h5>
        <h6 style="text-align: center;"><i><a href="<?= site_url('tiendaController/muestraregistro') ?>" style="text-decoration:none;" class="text-muted">Continuar sin iniciar sesión</a></i></h6>
        <div class="container" id="alineacion">
            <form method="post" action="<?= site_url("tiendaController/registro") ?>">

                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="first_name">Nombre</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Inserta Nombre">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="last_name">Apellidos</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Inserta ambos apellidos">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="username">Nombre de Usuario</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Inserta Nombre">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-phone"></i>&nbsp;<label name="phone">Teléfono</label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Inserta tu teléfono (sin prefijo)">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>&nbsp;<label name="email">Dirección de email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Inserta tu email">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-key"></i>&nbsp;<label name="password">Contraseña</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Inserta tu contraseña">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-key"></i>&nbsp;<label name="password2">Comprobación contraseña</label>
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Inserta de nuevo tu contraseña">
                </div> <br>
                <div class="custom-control custom-radio custom-control-inline" style="width:450px;">
                    <?= form_fieldset('¿A que grupo quieres pertenecer?*') ?>

                    <?= form_label('comprador', 'group', ['class' => 'control-label']) ?>
                    <?= form_radio('group', '3', FALSE, ['class' => 'custom-radio', 'id' => '3']) ?>

                    <?= form_label('vendedor', 'group', ['class' => 'control-label']) ?>
                    <?= form_radio('group', '4', FALSE, ['class' => 'custom-radio', 'id' => '4']) ?>

                    <?= form_label('compra-venta', 'group', ['class' => 'control-label']) ?>
                    <?= form_radio('group', '5', FALSE, ['class' => 'custom-radio', 'id' => '5']) ?>

                    <?= form_label('VIP', 'group', ['class' => 'control-label']) ?>
                    <?= form_radio('group', '6', FALSE, ['class' => 'custom-radio', 'id' => '6']) ?> 

                    <?= form_fieldset_close() ?><br><br>
                    <p>* Si no sabe que grupo elegir, le recomiendo que lea la siguiente guía:
                    <li>Comprador: perfil encarado únicamente a usuarios que quieran comprar en 2nXance.</li>
                    <li>Vendedor: perfil encarado únicamente a usuarios que quieran vender en 2nXance.</li>
                    <li>Compra-Venta: perfil encarado únicamente a usuarios que quieran comprar y vender en 2nXance. Si no sabe cuál elegir, le recomendamos este, debido a su polivalencia. En un futuro puede cambiar el grupo.</li>
                    <li>VIP: igual que el perfil Compra-Venta, pero de pago mensual (3.99 €) y gozan de diversos beneficios (poder posicionar sus productos, descuentos exlusivos, envío gratis...).</li>
                    </p>
                </div>
                <button type="submit" class="btn btn-warning" style="margin-left:44.5%;"><i class="fas fa-upload"></i>&nbsp; Regístrate</button>
            </form>
        </div>
    </body>
</html>

