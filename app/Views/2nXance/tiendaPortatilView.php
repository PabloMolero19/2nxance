<?= $this->extend('layout/plantilla2') ?>

<?= $this->section('content') ?>
<!-- Header-->
<header class="bg-secondary py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">Tienda de <span class="text text-warning">2nXance</span></h1>
            <p class="lead fw-normal text-white-50 mb-0">Encuentra todos los productos que necesitas en el mismo lugar</p>
        </div>
    </div>
</header>
<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <?php foreach ($portatil as $item): ?>
                <div class="col mb-5">
                    <div class="card h-100">
                        <!-- Product image-->
                        <img class="card-img-top" src="<?= base_url('assets/img/productos/portatil/' . $item['referencia']) ?>.jpg" width="150px">
                        <!-- Product details-->
                        <div class="card-body p-4">
                            <h5 class="fw-bolder"><?= $item['nom_comercial'] ?></h5>
                            <ul>
                                <li>CPU: <?= $item['cpu'] ?></li>
                                <li>RAM: <?= $item['ram'] ?></li>
                                <li>GPU: <?= $item['graficos'] ?></li>
                                <li>Pantalla: <?= $item['pantalla'] ?></li>
                                <li>Almacenamiento: <?= $item['almacenamiento'] ?></li>
                                <li>Sistema Operativo: <?= $item['so'] ?></li><br>
                                <!-- Product price-->
                                Precio: <?= $item['precio'] ?>&nbsp;€</ul>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><a href="<?= site_url('tiendaController/comprar/portatil/' . $item['portatil_id']) ?>" onclick="return confirm('Añadir a la cesta <?= $item['nom_comercial'] ?>, con referencia <?= $item['referencia'] ?>?')" class="btn btn-outline-success btn-sm"> añadir a la cesta</a></div><br>
                            <?php $this->auth = new \IonAuth\Libraries\IonAuth(); ?>
                            <?php if ($this->auth->loggedIn() AND ($this->auth->isAdmin())): ?>
                                <div class="text-center"><a href="<?= site_url('tiendaController/borrarPortatil/' . $item['portatil_id']) ?>"
                                                            onclick="return confirm('Vas a borrar a <?= $item['nom_comercial'] ?>, con id <?= $item['portatil_id'] ?>. Esta acción no se puede deshacer. ¿Estás seguro?')" class="btn btn-outline-danger btn-sm">borrar</a></div>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?= $this->endSection() ?>