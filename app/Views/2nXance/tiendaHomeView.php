<?php
$auth = new \IonAuth\Libraries\IonAuth();
?>

<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<?php if ($auth->loggedIn()): ?>
    <?php $user = $auth->user()->row(); ?>
    <br><br><br><br><br><br><br><br><br><br>
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Encantados de tenerte, <span class="text-info"><?= $user->first_name . ' ' . $user->last_name ?></span></h2>
                <h3 class="section-subheading text-muted">Puedes hacer lo siguiente:</h3>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('tiendaController/productos') ?>">Ver la tienda</a>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('tiendaController/contacto') ?>">Contactarnos</a>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('tiendaController/faq') ?>">Solicitar ayuda</a>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('auth/logout') ?>">Cerrar sesión</a><br><br><br><br>
            </div>

        </div>
    </section>
<?php else: ?>
    <br><br><br><br><br><br><br><br><br><br>
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Bienvenido a nuestra tienda</h2>
                <h3 class="section-subheading text-muted">Puedes acceder a la tienda de diferentes maneras. Elige una:</h3>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('tiendaController/muestraregistro') ?>">Registrarme</a>
                <a class="btn btn-warning btn-xl text-uppercase" href="<?= site_url('auth/login') ?>">Iniciar sesión</a><br><br><br><br>
                <a href="<?= site_url('tiendaController/productos') ?>" class="section-subheading text-muted"><h6><em>O continúa sin iniciar sesión</em></h6></a>
            </div>

        </div>
    </section>
<?php endif ?>
<?= $this->endSection() ?>
