<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<div class="text-center" style="padding-top: 2%">
    <h2 class="section-heading text-uppercase" ><span class="text-warning">2nXance</span> - Insertar</h2>
    <h4 class="section-subheading text-muted"><?= $titulo ?></h4>
</div><br>
<div class="container" style="margin: 0 auto; padding-left: 30%;">
    <?php if (!empty(\Config\Services::validation()->getErrors())): ?>
        <div class="alert alert-danger" role="alert">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
    <?php endif; ?>
    <?= form_open_multipart(site_url("tiendaController/insertarPeriferico")) ?>        
    <div class="form-group" style="width:450px;">
        <label name="modelo">Tipo</label>
        <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Por ejemplo Auriculares">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="marca">Marca</label>
        <input type="text" class="form-control" id="marca" name="marca" placeholder="Por ejemplo MSI">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="nom_comercial">Modelo</label>
        <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Por ejemplo Kraken">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="nom_comercial">Nombre Comercial</label>
        <input type="text" class="form-control" id="nom_comercial" name="nom_comercial" placeholder="Es la suma de los anteriores campos: Razer Kraken">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="precio">Precio en €</label>
        <input type="text" class="form-control" id="precio" name="precio" placeholder="Por ejemplo 500">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <?= form_upload(['name' => 'imagen', 'id' => 'imagen', 'class' => 'custom-file-control', 'onchange' => 'readURL(this);']) ?>
        <?= form_label('Selecciona la imagen', 'imagen', ['class' => 'custom-file-label']) ?>
    </div>
    <div class="form-group" style="width:450px;">
        <img id="visor" class=""  width="100%">
    </div>
</div>
<div style="margin-left:45%;" >
    <?= form_submit('Subir producto', 'Subir producto', ['class' => 'btn btn-warning']) ?>
</div><br>
<?= form_close() ?>
<a class="btn btn-outline-warning" style="margin-left:43.5%;" href="<?= site_url('tiendaController/productos') ?>">Volver al home de tienda</a><br><br>
<script type="text/javascript" src="<?= base_url('js/visor.js') ?>"></script>
<?= $this->endSection() ?>
