<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>

<div class="text-center" style="padding-top: 2%">
    <h2 class="section-heading text-uppercase" ><span class="text-warning">2nXance</span> - Insertar</h2>
    <h4 class="section-subheading text-muted"><?= $titulo ?></h4>
</div><br>
<div class="container" style="margin: 0 auto; padding-left: 30%;">
    <?php if (!empty(\Config\Services::validation()->getErrors())): ?>
        <div class="alert alert-danger" role="alert">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
    <?php endif; ?>

    <?= form_open_multipart(site_url("tiendaController/insertarContacto")) ?>

    <div class="form-group" style="width:450px;">
        <label name="modelo">Nombre de usuario</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Por ejemplo: Dark Hoodie 34">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="marca">Nombre</label>
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Por ejemplo: Luis">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="nom_comercial">Apellidos</label>
        <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Por ejemplo: García Berlanga">
    </div> <br>
    <div class="form-group" style="width:450px;">
        <label name="nom_comercial">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Por ejemplo: contacto@secondchance.com">
    </div> <br>
    <div style="margin-left:22%;" >
        <?= form_submit('Enviar', 'Enviar', ['class' => 'btn btn-warning']) ?>
    </div><br>
    <?= form_close() ?>
    <a class="btn btn-outline-warning" style="margin-left:16%;" href="<?= site_url('tiendaController/productos') ?>">Volver al home de tienda</a><br><br>
    <?= $this->endSection() ?>
