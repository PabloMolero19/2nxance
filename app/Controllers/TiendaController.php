<?php

//dónde se va a ubicar el archivo
namespace App\Controllers;

//referencias a los archivos que vamos a usar
use Config\Services;
use App\Models\SobremesaModel;
use App\Models\PortatilModel;
use App\Models\TabletaModel;
use App\Models\SmartphoneModel;
use App\Models\PerifericoModel;
use App\Models\RoboticaModel;
use App\Models\UsersModel;
use App\Models\ContactoModel;

//pre cargamos módulo de sesión
$session = \Config\Services::session();

class TiendaController extends BaseController {

    //definimos variables protegidas
    protected $auth;
    protected $session;
    protected $tiendaModel;
    protected $portatilModel;
    protected $tabletaModel;
    protected $perifericoModel;
    protected $smartphoneModel;
    protected $roboticaModel;
    protected $contactoModel;

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();
        $this->tiendaModel = new SobremesaModel();
        $this->portatilModel = new PortatilModel();
        $this->tabletaModel = new TabletaModel();
        $this->perifericoModel = new PerifericoModel();
        $this->smartphoneModel = new SmartphoneModel();
        $this->roboticaModel = new RoboticaModel();
        $this->contactoModel = new ContactoModel();
    }

    public function index() {

        $data ['pantalla'] = "Tienda";
        helper(['form']);
        echo view('2nXance/homeView2', $data);
    }

    public function tienda() {

        $data ['pantalla'] = "Home";
        $data ['titulo'] = "2nXance Tienda";
        echo view('2nXance/tiendaHomeView', $data);
    }

    public function productos() {

        $data ['pantalla'] = "Tienda";
        $data ['titulo'] = "2nXance Tienda";
        echo view('2nXance/tiendaView', $data);
    }

    public function muestraregistro() {

        $data ['pantalla'] = "Registro - 2nXance";
        helper(['form']);

        $data['registro'] = new UsersModel();

        echo view('2nXance/registroView', $data);
    }

    public function registro() {
        
        //forma de registrar de ion auth
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $email = $this->request->getPost('email');
        $additional_data = array(
            "first_name" => $this->request->getPost('first_name'),
            "lastname" => $this->request->getPost('last_name'),
        );
        $group = array(
            '2',
            $this->request->getPost('group')
        );
        
        //inserta si se han validado las variables 
        $resultado = $this->auth->register($username, $password, $email, $additional_data, $group);
        return redirect()->to('/tiendaController/muestraregistro/');
    }

    public function sobremesa() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);

        $data['sobremesa'] = $this->tiendaModel
                ->select('sobremesa_id, referencia,marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, graficos, precio')
                ->findAll();

        return view('2nXance/tiendaSobremesaView', $data);
    }

    public function insertarSobremesa() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "insertar";
            $data ['titulo'] = "Insertar nuevo producto de sobremesa";
            helper(['form']);

            $data['sobremesa'] = $this->tiendaModel
                    ->select('sobremesa_id, referencia,marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, graficos, precio')
                    ->findAll();

            //echo $this->request->getMethod();

            if ($this->request->getMethod() == 'post') {
                //echo "estoy en el post";
                if ($this->validate($this->tiendaModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
                    //echo "he validado";
                    $producto = [
                        "referencia" => $randomreferencia,
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "so" => $this->request->getPost('so'),
                        "cpu" => $this->request->getPost('cpu'),
                        "ram" => $this->request->getPost('ram'),
                        "almacenamiento" => $this->request->getPost('almacenamiento'),
                        "graficos" => $this->request->getPost('graficos'),
                        "precio" => $this->request->getPost('precio'),
                    ];
                    
                    //prueba a subir la imagen, si hay una excepción la muestra y si no ha podido subirla, muestra que no se ha podido y no la sube
                    
                    try {
                        $path = $this->request->getFile('imagen')->store('sobremesa/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->tiendaModel->insert($producto);
            }

            return view('2nXance/tiendaInsertarSobremesaView', $data);
        }
    }

    public function borrarSobremesa($sobremesa_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new SobremesaModel();
            
            //borra mediante el id de sobremesa
            $tiendaModel->delete($sobremesa_id);
            return redirect()->to('/tiendaController/sobremesa/');
        } else {

            echo 'no puedes, no eres administrador';
        }
    }

    public function portatil() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);

        $data['portatil'] = $this->portatilModel
                ->select('portatil_id, referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, graficos, precio')
                ->findAll();

        /* echo '<pre>';
          print_r($data);
          echo '</pre>'; */

        //echo view('plantilla/2nXance/header', $data);
        return view('2nXance/tiendaPortatilView', $data);
        //echo view('plantilla/2nXance/footer', $data);
    }

    public function insertarPortatil() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "insertar";
            $data ['titulo'] = "Insertar nuevo producto de pc portatil";
            helper(['form']);

            $data['portatil'] = $this->portatilModel
                    ->select('portatil_id, referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, graficos, precio')
                    ->findAll();

            if ($this->request->getMethod() == 'post') {
                //echo "estoy en el post";
                if ($this->validate($this->portatilModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
                    //echo "he validado";

                    $producto = [
                        "referencia" => $randomreferencia,
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "so" => $this->request->getPost('so'),
                        "cpu" => $this->request->getPost('cpu'),
                        "ram" => $this->request->getPost('ram'),
                        "almacenamiento" => $this->request->getPost('almacenamiento'),
                        "pantalla" => $this->request->getPost('pantalla'),
                        "graficos" => $this->request->getPost('graficos'),
                        "precio" => $this->request->getPost('precio'),
                    ];

                    try {
                        $path = $this->request->getFile('imagen')->store('portatil/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->portatilModel->insert($producto);
            }
            return view('2nXance/tiendaInsertarPortatilView', $data);
        }
    }

    public function borrarPortatil($portatil_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new PortatilModel();

            //$tiendaModel->delete($portatil_id);
            $tiendaModel->where('portatil_id', $portatil_id)->delete();
            return redirect()->to('/tiendaController/portatil/');
        } else {

            echo 'que no puedes, está todo probado';
        }
    }

    public function tableta() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);


        $data['tableta'] = $this->tabletaModel
                ->select('tableta_id,referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                ->findAll();

        /* echo '<pre>';
          print_r($data);
          echo '</pre>'; */

        //echo view('plantilla/2nXance/header', $data);
        return view('2nXance/tiendaTabletaView', $data);
        //echo view('plantilla/2nXance/footer', $data);
    }

    public function insertarTableta() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "insertar";
            $data ['titulo'] = "Insertar nuevo producto de tableta";
            helper(['form']);

            $data['tableta'] = $this->tabletaModel
                    ->select('tableta_id,referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                    ->findAll();
            if ($this->request->getMethod() == 'post') {
                //echo "estoy en el post";
                if ($this->validate($this->tabletaModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
                    //echo "he validado";

                    $producto = [
                        "referencia" => $randomreferencia,
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "so" => $this->request->getPost('so'),
                        "cpu" => $this->request->getPost('cpu'),
                        "ram" => $this->request->getPost('ram'),
                        "almacenamiento" => $this->request->getPost('almacenamiento'),
                        "pantalla" => $this->request->getPost('pantalla'),
                        "bateria" => $this->request->getPost('bateria'),
                        "precio" => $this->request->getPost('precio'),
                    ];

                    try {
                        $path = $this->request->getFile('imagen')->store('tableta/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->tabletaModel->insert($producto);
            }

            return view('2nXance/tiendaInsertarTabletaView', $data);
        }
    }

    public function borrarTableta($tableta_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new TabletaModel();

            //$tiendaModel->delete($portatil_id);
            $tiendaModel->where('tableta_id', $tableta_id)->delete();
            return redirect()->to('/tiendaController/tableta/');
        } else {

            echo 'que no puedes, está todo probado';
        }
    }

    public function smartphones() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);

        $data['smartphones'] = $this->smartphoneModel
                ->select('movil_id, referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                ->findAll();

        /* echo '<pre>';
          print_r($data);
          echo '</pre>'; */

        //echo view('plantilla/2nXance/header', $data);
        return view('2nXance/tiendaSmartphonesView', $data);
        //echo view('plantilla/2nXance/footer', $data);
    }

    public function insertarSmartphones() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "Tienda";
            $data ['titulo'] = "Insertar nuevo producto de smartphone";
            helper(['form']);

            $data['smartphones'] = $this->smartphoneModel
                    ->select('movil_id, referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                    ->findAll();

            if ($this->request->getMethod() == 'post') {
//echo "estoy en el post";
                if ($this->validate($this->smartphoneModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
//echo "he validado";
                    $producto = [
                        "referencia" => $randomreferencia,
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "so" => $this->request->getPost('so'),
                        "cpu" => $this->request->getPost('cpu'),
                        "ram" => $this->request->getPost('ram'),
                        "almacenamiento" => $this->request->getPost('almacenamiento'),
                        "pantalla" => $this->request->getPost('pantalla'),
                        "bateria" => $this->request->getPost('bateria'),
                        "precio" => $this->request->getPost('precio'),
                    ];

                    try {
                        $path = $this->request->getFile('imagen')->store('smartphone/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->smartphoneModel->insert($producto);
            }
            return view('2nXance/tiendaInsertarSmartphonesView', $data);
        }
    }

    public function borrarSmartphones($movil_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new SmartphoneModel();

            //$tiendaModel->delete($portatil_id);
            $tiendaModel->where('movil_id', $movil_id)->delete();
            return redirect()->to('/tiendaController/smartphones/');
        } else {

            echo 'que no puedes, está todo probado';
        }
    }

    public function periferico() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);


        $data['periferico'] = $this->perifericoModel
                ->select('periferico_id, referencia, tipo, marca, modelo, nom_comercial, precio')
                ->findAll();

        /* echo '<pre>';
          print_r($data);
          echo '</pre>'; */

        //echo view('plantilla/2nXance/header', $data);
        return view('2nXance/tiendaPerifericoView', $data);
        //echo view('plantilla/2nXance/footer', $data);
    }

    public function insertarPeriferico() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "Tienda";
            $data ['titulo'] = "Insertar nuevo producto de periferico";
            helper(['form']);

            $data['periferico'] = $this->perifericoModel
                    ->select('periferico_id, referencia, tipo, marca, modelo, nom_comercial, precio')
                    ->findAll();

            if ($this->request->getMethod() == 'post') {
                //echo "estoy en el post";
                if ($this->validate($this->perifericoModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
//echo "he validado";
                    $producto = [
                        "referencia" => $randomreferencia,
                        "tipo" => $this->request->getPost('tipo'),
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "precio" => $this->request->getPost('precio'),
                    ];

                    try {
                        $path = $this->request->getFile('imagen')->store('periferico/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->perifericoModel->insert($producto);
            }

            return view('2nXance/tiendaInsertarPerifericoView', $data);
        }
    }

    public function borrarPeriferico($periferico_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new PerifericoModel();

            //$tiendaModel->delete($portatil_id);
            $tiendaModel->where('periferico_id', $periferico_id)->delete();
            return redirect()->to('/tiendaController/periferico/');
        } else {

            echo 'que no puedes, está todo probado';
        }
    }

    public function robotica() {
        $data ['pantalla'] = "Tienda";
        //helper(['form']);;

        $data['robotica'] = $this->roboticaModel
                ->select('robotica_id, referencia, tipo, modelo, nom_comercial, precio')
                ->findAll();

        /* echo '<pre>';
          print_r($data);
          echo '</pre>'; */

        //echo view('plantilla/2nXance/header', $data);
        return view('2nXance/tiendaRoboticaView', $data);
        //echo view('plantilla/2nXance/footer', $data);
    }

    public function insertarRobotica() {
        if ($this->auth->loggedIn() AND ($this->auth->isAdmin() OR $this->auth->inGroup('vendedor') OR $this->auth->inGroup('compraventa') OR $this->auth->inGroup('VIP'))) {
            $data ['pantalla'] = "Tienda";
            $data ['titulo'] = "Insertar nuevo producto de robótica";
            helper(['form']);

            $data['robotica'] = $this->roboticaModel
                    ->select('robotica_id, referencia, tipo, modelo, nom_comercial, precio')
                    ->findAll();

            if ($this->request->getMethod() == 'post') {
                //echo "estoy en el post";
                if ($this->validate($this->roboticaModel->getValidationRules())) {
                    $randomreferencia = rand(10000, 99999);
//echo "he validado";
                    $producto = [
                        "referencia" => $randomreferencia,
                        "tipo" => $this->request->getPost('tipo'),
                        "marca" => $this->request->getPost('marca'),
                        "modelo" => $this->request->getPost('modelo'),
                        "nom_comercial" => $this->request->getPost('nom_comercial'),
                        "precio" => $this->request->getPost('precio'),
                    ];

                    try {
                        $path = $this->request->getFile('imagen')->store('robotica/', $producto['referencia'] . '.jpg');
                        rename(WRITEPATH . 'uploads/' . $path, ROOTPATH . 'public/assets/img/productos/' . $path);
                    } catch (Exception $e) {
                        echo 'Excepción capturada: ', $e->getMessage(), "\n";
                    }
                } else {
                    echo "NO se ha podido subir la imagen";
                    var_dump($this->validator->getErrors());
                }

                $this->roboticaModel->insert($producto);
            }

            return view('2nXance/tiendaInsertarRoboticaView', $data);
        }
    }

    public function borrarRobotica($robotica_id) {
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()) {
            $tiendaModel = new PerifericoModel();

            //$tiendaModel->delete($robotica_id);
            $tiendaModel->where('robotica_id', $robotica_id)->delete();
            return redirect()->to('/tiendaController/robotica/');
        } else {

            echo 'que no puedes, está todo probado';
        }
    }

    public function comprar($tipo, $id) {
        if ($this->auth->loggedIn()) {
            if ($this->session->has('carro')) {
                //existe el carro
                $carro = $this->session->get('carro');
                if (isset($carro[$tipo][$id])) {
                    // si existe el elemento, incrementa su cantidad
                    $carro[$tipo][$id]++;
                } else {
                    $carro[$tipo][$id] = 1; //inicio la cantidad si no existe
                }
            } else {
                //crear el carro
                $carro[$tipo][$id] = 1; //inicio la cantidad sino existe
            }

            //y en todos los casos guardar la variable de sesion, carro
            $this->session->set('carro', $carro);
            return redirect()->to(site_url('tiendaController/' . $tipo));
        }
    }

    public function muestracarro() {
        if ($this->auth->loggedIn()) {
            //error_reporting(E_ERROR|E_WARNING); //esto me omite los errores y carga sin ellos

            $data ['pantalla'] = "carrito";
            $data ['titulo'] = "productos en el carrito";
            $sobremesa = $portatil = $tableta = $periferico = $smartphone = $robotica = FALSE;

            $carro = $this->session->carro;
            //para cada tipo de componente dentro de la variable carro, iremos añadiendo al carro, siempre que la variable sea TRUE,
            //si no, no lo inserta y no da error porque no tiene valor la variable
            foreach ($carro as $tipo => $tipocomponente) {
                switch ($tipo) {
                    case 'sobremesa':
                        $sobremesa = TRUE;
                        $data['sobremesa'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['sobremesa'][$id] = $this->tiendaModel
                                    ->select('referencia,marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, graficos, precio')
                                    ->where(['sobremesa_id' => $id])
                                    ->first();
                            $data['sobremesa'][$id]['cantidad'] = $cantidad;
                        }
                        break;
                    case 'portatil':
                        $portatil = TRUE;
                        $data['portatil'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['portatil'][$id] = $this->portatilModel
                                    ->select('referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, graficos, precio')
                                    ->where(['portatil_id' => $id])
                                    ->first();
                            $data['portatil'][$id]['cantidad'] = $cantidad;
                        }
                        break;
                    case 'tableta':
                        $tableta = TRUE;
                        $data['tableta'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['tableta'][$id] = $this->tabletaModel
                                    ->select('referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                                    ->where(['tableta_id' => $id])
                                    ->first();
                            $data['tableta'][$id]['cantidad'] = $cantidad;
                        }
                        break;

                    case 'periferico':
                        $periferico = TRUE;
                        $data['periferico'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['periferico'][$id] = $this->perifericoModel
                                    ->select('periferico_id, referencia, tipo, marca, modelo, nom_comercial, precio')
                                    ->where(['periferico_id' => $id])
                                    ->first();

                            $data['periferico'][$id]['cantidad'] = $cantidad;
                        }
                        break;

                    case 'smartphones':
                        $smartphone = TRUE;
                        $data['smartphone'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['smartphone'][$id] = $this->smartphoneModel
                                    ->select('referencia, marca, modelo, nom_comercial, so, cpu, ram, almacenamiento, pantalla, bateria, precio')
                                    ->where(['movil_id' => $id])
                                    ->first();

                            $data['smartphone'][$id]['cantidad'] = $cantidad;
                        }
                        break;

                    case 'robotica':
                        $robotica = TRUE;
                        $data['robotica'] = [];
                        foreach ($tipocomponente as $id => $cantidad) {
                            $data['robotica'][$id] = $this->roboticaModel
                                    ->select('robotica_id, referencia, tipo, modelo, nom_comercial, precio')
                                    ->where(['robotica_id' => $id])
                                    ->first();

                            $data['robotica'][$id]['cantidad'] = $cantidad;
                        }
                        break;
                }
            }
            
            //concatenación de las vistas siempre que las variables tengan contenido
            $html = view('2nXance/carritoView', $data);
            if ($sobremesa == TRUE) {
                $html .= view('2nXance/carritoView_sobremesa', $data);
            }
            if ($portatil == TRUE) {
                $html .= view('2nXance/carritoView_portatil', $data);
            }
            if ($tableta == TRUE) {
                $html .= view('2nXance/carritoView_tableta', $data);
            }

            if ($periferico == TRUE) {
                $html .= view('2nXance/carritoView_periferico', $data);
            }

            if ($smartphone == TRUE) {
                $html .= view('2nXance/carritoView_smartphone', $data);
            }

            if ($robotica == TRUE) {
                $html .= view('2nXance/carritoView_robotica', $data);
            }



            return $html . view('2nXance/carritoViewFooter', $data);
        }
    }

    public function borraCarro() {
        if ($this->auth->loggedIn()) {
            $this->session->remove('carro'); //borra la variable de sesión
            return redirect()->to(site_url('/tiendaController/productos'));
        }
    }

    public function contacto() {
        $data ['pantalla'] = "Contacto";
        $data ['titulo'] = "Contacta con 2nXance";
        helper(['form']);
        return view('2nXance/contactoView', $data);
    }

    public function insertarContacto() {

        $data ['pantalla'] = "Contacto";
        $data ['titulo'] = "Contacta con 2nXance";
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
            if ($this->validate($this->contactoModel->getValidationRules())) {

                $consulta = [
                    "username" => $this->request->getPost('username'),
                    "nombre" => $this->request->getPost('nombre'),
                    "apellido" => $this->request->getPost('apellido'),
                    "email" => $this->request->getPost('email'),
                ];
            } else {
                var_dump($this->validator->getErrors());
            }

            $this->contactoModel->insert($consulta);
        }

        return view('2nXance/contactoView', $data);
    }

    public function faq() {
        $data ['pantalla'] = "FAQ";
        $data ['titulo'] = "Ayuda de 2nxance";
        return view('2nXance/faqView', $data);
    }

}
