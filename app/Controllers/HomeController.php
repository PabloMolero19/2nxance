<?php

namespace App\Controllers;

/*
 * Description of PruebaController
 *
 * @author PabloMolero
 */

class HomeController extends BaseController {

    public function index() {

        $data ['pantalla'] = "Home";
        helper(['form']);

        echo view('plantilla/2nXance/header', $data);
        echo view('2nXance/homeView');
        echo view('plantilla/2nXance/footer', $data);
    }

}
