<?php namespace Config;

class IonAuth extends \IonAuth\Config\IonAuth
{
    // set your specific config
    public $siteTitle                = 'Proyecto final 2nXance';       // Site Title
    // public $adminEmail               = 'admin@secondchance.com'; // Admin Email
    public $emailTemplates           = 'App\\Views\\auth\\email\\';
    // ...
}
